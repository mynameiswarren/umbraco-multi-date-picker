﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web;

namespace RB.MultiDatePicker.Converters
{
    /// <summary>
    /// Value converter class to convert a json multi date picker value object
    /// to a list of dates.
    /// </summary>
    [PropertyValueType(typeof(List<DateTime>))]
    [PropertyValueCache(PropertyCacheValue.All, PropertyCacheLevel.Content)]
    public class MultiDatePickerValueConvertor : PropertyValueConverterBase
    {
        /// <summary>
        /// Method to convert a property value to a list of dates.
        /// </summary>
        /// <param name="propertyType">The current published property
        /// type to convert.</param>
        /// <param name="source">The original property data.</param>
        /// <param name="preview">True if in preview mode.</param>
        /// <returns>A list of dates.</returns>
        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source == null)
                return null;

            return UmbracoContext.Current == null ? null : JsonConvert.DeserializeObject<List<DateTime>>(source.ToString());
        }

        /// <summary>
        /// Method to see if the current property type is of type
        /// multi date picker.
        /// </summary>
        /// <param name="propertyType">The current property type.</param>
        /// <returns>True if the current property type is of type
        /// multi date picker.</returns>
        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("RB.MultiDatePicker");
        }
    }
}