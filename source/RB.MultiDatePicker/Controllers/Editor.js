﻿angular.module("umbraco").controller("RB.MultiDatePicker", function ($scope, dialogService) {

    $scope.model.value = $scope.model.value || [];

    if (!($scope.model.value instanceof Array))
        $scope.model.value = [];

    $scope.addDate = function () {

        dialogService.open({ template: '/App_Plugins/RB.MultiDatePicker/Resource/Dialog.html', show: true, callback: done, dialogData: null });

        function done(data) {
            if (data)
                $scope.model.value.push(data);
        }
    };

    $scope.deleteDate = function (item) {
        $scope.model.value.pop(item);
    };

});