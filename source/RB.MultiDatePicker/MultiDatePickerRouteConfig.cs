﻿using System.Web.Mvc;
using System.Web.Routing;

namespace RB.MultiDatePicker
{
    public class MultiDatePickerRouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "MultiDatePicker",
                url: "App_Plugins/RB.MultiDatePicker/{action}/{id}",
                defaults: new { controller = "MultiDatePicker", action = "Resource", id = UrlParameter.Optional }
                );
        }
    }
}